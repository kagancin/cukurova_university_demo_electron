const {
    ipcRenderer: ipc,
    dialog,
    remote
} = require('electron');

(() =>  {

    const btnStart = document.querySelector('#btn_start');
    const btnEnd = document.querySelector('#btn_end');
    const btnQuit = document.querySelector('#btn_quit');
    const playerCount = document.querySelector('#player_count');

    ipc.on('game_state', (event, state) =>  {
        if (state === true) {
            btnStart.classList.add('hidden');
            btnEnd.classList.remove('hidden');
        } else {
            btnStart.classList.remove('hidden');
            btnEnd.classList.add('hidden');
        }
    });

    ipc.on('player_count', (event, count) =>  {
        playerCount.textContent = count;
    });

    btnStart.addEventListener('click', () => {
        ipc.send('start');
    });

    btnEnd.addEventListener('click', () => {
        ipc.send('end');
    });

    btnQuit.addEventListener('click', () => {
        remote.app.quit();
    });

})();