const electron = require('electron')
const {
    app,
    BrowserWindow,
    dialog,
    ipcMain: ipc
} = electron;
const path = require('path')
const url = require('url')
const io = require('socket.io-client');

let win

function createWindows() {

    let screen = electron.screen.getPrimaryDisplay();

    let {
        width,
        height
    } = screen.size;

    let {
        x,
        y
    } = screen.bounds;

    win = new BrowserWindow({
        width,
        height,
        titleBarStyle: 'hiddenInset',
        kiosk: false,
        closable: true,
        resizable: true,
        alwaysOnTop: false,
        fullscreen: true
    });

    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));

    app.focus();

    // win.webContents.openDevTools();

    win.on('closed', () => {
        win = null
    });

    win.webContents.on('did-finish-load', () =>  {

        const socket = io('https://www.cin.party/admin', {
            path: '/demo_socket/'
        });

        socket.connect();

        socket.on('game_state', (state, winner, answer) =>  {
            win.webContents.send('game_state', state);

            if (winner && answer) {
                ipc.emit('show-dialog', winner, answer);
            }
        });

        socket.on('player_count', (count) =>  {
            win.webContents.send('player_count', count);
        });

        ipc.on('start', () =>  {
            socket.emit('start');
        });

        ipc.on('end', () =>  {
            socket.emit('end');
        });

    })

}

ipc.on('show-dialog', (player, answer) => {
    dialog.showMessageBox(win, {
        type: 'info',
        buttons: ['OK'],
        message: `User ${player.username} won the game with a score of ${player.chance}. The answer was ${answer}.`
    });
});

app.on('ready', createWindows);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (win === null) {
        createWindows();
    }
});